package nonogram.persistance;

import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;

public class LevelLoaderTest {

    @Test
    public void testLevelLoad() {
        final InputStream resource = getClass().getClassLoader().getResourceAsStream("level_loader/level_01");
        final Level level = new LevelLoader().loadLevel(resource);

        final int[][] expectedLevelData = {
                {0, 1, 0},
                {1, 0, 1},
                {1, 0, 0}
        };
        Assert.assertArrayEquals(expectedLevelData, level.getLevelData());

        final int[][] expectedRowInformation = {
                {1},
                {1, 1},
                {1}
        };
        Assert.assertArrayEquals(expectedRowInformation, level.getRowInformation());

        final int[][] expectedColInformation = {
                {2},
                {1},
                {1}
        };
        Assert.assertEquals(expectedColInformation, level.getColInformation());
    }

}
