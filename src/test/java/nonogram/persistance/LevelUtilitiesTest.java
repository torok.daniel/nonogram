package nonogram.persistance;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LevelUtilitiesTest {

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][] {
                { new int[]{1}, new int[]{1} },
                { new int[]{1, 1, 0, 1}, new int[]{2,1} },
                { new int[]{0, 1, 1, 0, 1}, new int[]{2,1} },
                { new int[]{0, 1, 1, 0, 0}, new int[]{2} }
        });
    }

    private final int[] input;
    private final int[] expectedOutput;
    public LevelUtilitiesTest(final int[] input, final int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Test
    public void testGetNumberOfContinuousCells() {
        final int[] actualOutput = LevelUtilities.getNumberOfContinuousCells(input);
        Assert.assertArrayEquals(expectedOutput, actualOutput);
    }

}
