package nonogram.model;

import nonogram.persistance.Level;
import org.junit.Test;

import java.util.function.Supplier;

import static org.mockito.Mockito.*;

public class GameModelTest {

    @Test
    public void testGameModelCellSelection() {
        final Level level = createLevelVersion_1();
        final GameModelActionListener actionListener = mock(GameModelActionListener.class);
        final GameModel gameModel = new GameModel(level, actionListener);
        gameModel.start();

        // Test toggle function
        gameModel.onCellSelected(0, 0);
        verify(actionListener, times(1)).onCellSelected(0, 0, 1);
        gameModel.onCellSelected(0, 0);
        verify(actionListener, times(1)).onCellSelected(0, 0, 0);

        // Test edges
        gameModel.onCellSelected(0, 1);
        verify(actionListener, times(1)).onCellSelected(0, 1, 1);

        // Test edges
        gameModel.onCellSelected(1, 1);
        verify(actionListener, times(1)).onCellSelected(1, 1, 1);
    }

    @Test
    public void testGameModelPlayerWon() {
        final long firstTime = 10L;
        final long secondTime = 100L;
        final long elapsedTime = secondTime - firstTime;
        final Supplier<Long> timeSupplier = mock(Supplier.class);
        when(timeSupplier.get())
                .thenReturn(firstTime)
                .thenReturn(secondTime);

        final Level level = createLevelVersion_1();
        final GameModelActionListener actionListener = mock(GameModelActionListener.class);
        final GameModel gameModel = new GameModel(level, actionListener, timeSupplier);
        gameModel.start(); // sets start time

        gameModel.onCellSelected(0, 1);
        verify(actionListener, times(0)).onPlayerWon(anyLong());
        gameModel.onCellSelected(1, 0);
        verify(actionListener, times(1)).onPlayerWon(elapsedTime);
    }

    private Level createLevelVersion_1() {
        final int[][] levelData = {
                {0, 1},
                {1, 0}
        };
        final int[][] information = {
                {1},
                {1}
        };
        return new Level(levelData, information, information);
    }

}
