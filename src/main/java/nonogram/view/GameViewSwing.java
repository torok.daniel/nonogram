package nonogram.view;

import nonogram.model.GameModelActionListener;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class GameViewSwing implements GameModelActionListener {

    enum State {
        EMPTY, FILLED, HIGHLIGHTED, INFORMATION
    }

    private final static Map<State, Color> colorMappings = new HashMap<>();
    static {
        colorMappings.put(State.EMPTY, Color.WHITE);
        colorMappings.put(State.FILLED, Color.BLACK);
        colorMappings.put(State.HIGHLIGHTED, Color.LIGHT_GRAY);
        colorMappings.put(State.INFORMATION, Color.DARK_GRAY);
    }

    private final int cellSize;
    private PointSupplier[][] buttonCoordinateSupplier;
    private JButton[][] allButton;
    private JButton[][] clickableButtons;

    public GameViewSwing(final int cellSize) {
        this.cellSize = cellSize;
    }

    public JButton[][] getClickableButtons() {
        return this.clickableButtons;
    }

    @Override
    public void onCellSelected(final int rowIdx, final int colIdx, final int newValue) {
        final Color newColor = getColor(newValue);
        clickableButtons[rowIdx][colIdx].setBackground(newColor);
    }

    @Override
    public void onCellHighlighted(final int rowIdx, final int colIdx) {
        clearHighlights(allButton);
        final Point point = buttonCoordinateSupplier[rowIdx][colIdx].get();

        // highlight row
        for (final JButton colButton : allButton[point.getRow()]) {
            highlightButton(colButton);
        }

        // highlight column
        for (final JButton[] row : allButton) {
            highlightButton(row[point.getColumn()]);
        }
    }

    @Override
    public void onPlayerWon(final long elapsedTime) {
        clearHighlights(allButton);
        JOptionPane.showMessageDialog(null, "You WIN! Time elapsed: " + elapsedTime/1000 + " seconds.");
    }

    @Override
    public void onDrawInitialLevel(final int[][] rowInformation, final int[][] colInformation) {
        final int width = colInformation.length;
        final int height = rowInformation.length;
        final int rowInformationLength = getSizeOfLongestSubList(rowInformation);
        final int colInformationLength = getSizeOfLongestSubList(colInformation);
        final int gridWidth = width + rowInformationLength;
        final int gridHeight = height + colInformationLength;

        final JFrame gameWindow = new JFrame();
        gameWindow.setSize(gridWidth * cellSize, gridHeight * cellSize);
        gameWindow.setLayout(new GridLayout(gridHeight, gridWidth, 2, 2));

        final int[][] levelWithInformation = new int[gridHeight][gridWidth];
        copyColInformation(colInformation, rowInformationLength, colInformationLength, levelWithInformation);
        copyRowInformation(rowInformation, rowInformationLength, colInformationLength, levelWithInformation);

        allButton = new JButton[gridHeight][gridWidth];
        clickableButtons = new JButton[height][width];
        buttonCoordinateSupplier = new PointSupplier[gridHeight][gridWidth];

        for (int rowIdx = 0; rowIdx < gridHeight; rowIdx++) {
            for (int colIdx = 0; colIdx < gridWidth; colIdx++) {

                final int data = levelWithInformation[rowIdx][colIdx];
                final JButton button = createButtonForData(data);
                allButton[rowIdx][colIdx] = button;
                gameWindow.add(button);

                // check if we are adding the buttons that the player can interact with
                if (rowIdx >= colInformationLength && colIdx >= rowInformationLength) {
                    final int absoluteRowIdx = rowIdx;
                    final int absoluteColIdx = colIdx;
                    final int relativeRowIdx = absoluteRowIdx - colInformationLength;
                    final int relativeColIdx = absoluteColIdx - rowInformationLength;
                    buttonCoordinateSupplier[relativeRowIdx][relativeColIdx] = () -> new Point(absoluteRowIdx, absoluteColIdx);
                    clickableButtons[relativeRowIdx][relativeColIdx] = button;
                }
            }
        }

        gameWindow.setVisible(true);
    }

    void copyColInformation(final int[][] colInformation, final int widthExtension, final int heightExtension, final int[][] target) {
        for (int colIdx = 0; colIdx < colInformation.length; colIdx++) {
            final int[] col = colInformation[colIdx];
            int targetRowIdx = heightExtension - col.length;
            int targetColIdx = colIdx + widthExtension;

            for (int infoIdx = 0; infoIdx < col.length; infoIdx++) {
                target[targetRowIdx + infoIdx][targetColIdx] = col[infoIdx];
            }
        }
    }

    void copyRowInformation(final int[][] rowInformation, final int widthExtension, final int heightExtension, final int[][] target) {
        for (int rowIdx = 0; rowIdx < rowInformation.length; rowIdx++) {
            final int[] row = rowInformation[rowIdx];
            int targetRowIdx = rowIdx + heightExtension;
            int targetColIdx = widthExtension - row.length;

            System.arraycopy(row, 0, target[targetRowIdx], targetColIdx, row.length);
//          SAME AS:
//            for (int infoIdx = 0; infoIdx < row.length; infoIdx++) {
//                target[targetRowIdx][targetColIdx + infoIdx] = row[infoIdx];
//            }
        }
    }

    int getSizeOfLongestSubList(final int[][] list) {
        return Stream.of(list).mapToInt(l -> l.length).max().orElse(0);
    }

    void clearHighlights(final JButton[][] buttons) {
        Stream.of(buttons).forEach(row -> Stream.of(row).forEach(this::resetButtonHighlight));
    }

    void resetButtonHighlight(final JButton button) {
        if (button.getBackground().equals(colorMappings.get(State.HIGHLIGHTED))) {
            button.setBackground(colorMappings.get(State.INFORMATION));
        }
    }

    void highlightButton(final JButton button) {
        if (button.getBackground().equals(colorMappings.get(State.INFORMATION))) {
            button.setBackground(colorMappings.get(State.HIGHLIGHTED));
        }
    }

    JButton createButtonForData(final int cellData) {
        final JButton button = new JButton();
        if (cellData > 0) {
            button.setText("" + cellData);
            button.setForeground(colorMappings.get(State.EMPTY));
            button.setBackground(colorMappings.get(State.INFORMATION));
        } else {
            button.setBackground(colorMappings.get(State.EMPTY));
        }
        return button;
    }

    Color getColor(final int cellData) {
        if (cellData == 0) {
            return colorMappings.get(State.EMPTY);
        } else {
            return colorMappings.get(State.FILLED);
        }
    }

}
