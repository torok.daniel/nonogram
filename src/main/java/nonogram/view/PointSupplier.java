package nonogram.view;

import java.util.function.Supplier;

public interface PointSupplier extends Supplier<Point> {
}
