package nonogram;

import nonogram.controller.GameControllerSwingMouse;
import nonogram.model.GameModel;
import nonogram.persistance.Level;
import nonogram.persistance.LevelLoader;
import nonogram.view.GameViewSwing;

import java.io.InputStream;

public class App {

    private final static int CELL_SIZE = 100;

    public static void main(String[] args) {
        new App().doMain();
    }

    private void doMain() {

        // level_01
        // https://www.nonograms.org/nonograms/i/4353
        //
        // level_02
        // https://www.nonograms.org/nonograms/i/6507

        final InputStream levelInputStream = getClass().getClassLoader().getResourceAsStream("levels/level_02");
        final Level level = new LevelLoader().loadLevel(levelInputStream);

        final GameViewSwing gameView = new GameViewSwing(CELL_SIZE);
        final GameModel gameModel = new GameModel(level, gameView);
        gameModel.start();

        final GameControllerSwingMouse gameController = new GameControllerSwingMouse(gameView.getClickableButtons(), gameModel);
        gameController.start();
    }

}
