package nonogram.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public interface GameControllerSwingMouseListener extends MouseListener {

    // only interface method needed, rest of them have default no-op implementation
    void mouseEntered(MouseEvent e);

    @Override
    default void mouseClicked(MouseEvent e) {

    }

    @Override
    default void mousePressed(MouseEvent e) {

    }

    @Override
    default void mouseReleased(MouseEvent e) {

    }

    @Override
    default void mouseExited(MouseEvent e) {

    }

}
