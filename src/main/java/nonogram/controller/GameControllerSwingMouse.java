package nonogram.controller;

import nonogram.model.GameModel;

import javax.swing.*;

public class GameControllerSwingMouse {

    private final JButton[][] clickableButtons;
    private final GameModel gameModel;

    public GameControllerSwingMouse(final JButton[][] clickableButtons, final GameModel gameModel) {
        this.clickableButtons = clickableButtons;
        this.gameModel = gameModel;
    }

    public void start() {
        for (int rowIdx = 0; rowIdx < clickableButtons.length; rowIdx++) {
            final JButton[] row = clickableButtons[rowIdx];
            for (int colIdx = 0; colIdx < row.length; colIdx++) {
                final int localRowIdx = rowIdx;
                final int localColIdx = colIdx;
                final JButton button = row[colIdx];
                button.addActionListener(e -> gameModel.onCellSelected(localRowIdx, localColIdx));
                button.addMouseListener((GameControllerSwingMouseListener) e -> gameModel.onCellHighlighted(localRowIdx, localColIdx));
            }
        }
    }

}
