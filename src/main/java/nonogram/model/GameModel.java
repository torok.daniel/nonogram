package nonogram.model;

import nonogram.persistance.Level;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class GameModel {

    private final Level level;
    private final GameModelActionListener actionListener;
    private final Supplier<Long> timeSupplier;

    private long startTime = 0L;
    private boolean isGameOver = false;
    private int numberOfCellsToFill = 0;
    private int[][] currentLevelView;

    public GameModel(final Level level, final GameModelActionListener actionListener) {
        this(level, actionListener, System::currentTimeMillis);
    }

    public GameModel(final Level level, final GameModelActionListener actionListener, final Supplier<Long> timeSupplier) {
        this.level = level;
        this.actionListener = actionListener;
        this.timeSupplier = timeSupplier;
    }

    public void start() {
        currentLevelView = new int[level.getHeight()][level.getWidth()];
        numberOfCellsToFill = countNumberOfFilledCells(level.getLevelData());
        actionListener.onDrawInitialLevel(level.getRowInformation(), level.getColInformation());

        isGameOver = false;
        startTime = timeSupplier.get();
    }

    public void onCellSelected(int rowIdx, int colIdx) {
        if (isGameOver) {
            return;
        }

        final int referenceValue = level.getLevelData()[rowIdx][colIdx];
        final int currentValue = currentLevelView[rowIdx][colIdx];
        // (XOR 1) toggles the value between 0 and 1
        final int newValue = currentValue ^ 1;
        currentLevelView[rowIdx][colIdx] = newValue;
        actionListener.onCellSelected(rowIdx, colIdx, newValue);

//        if newValue is 1 and referenceValue is 1 then the player is one step closer to the solution
//        if newValue is 0 and referenceValue is 1 then the player in one step farther from the solution
//
//        if newValue is 1 and referenceValue is 0 then the player is one step farther from the solution
//        if newValue is 0 and referenceValue is 0 then the player is one step closer to the solution
//
//        ONE LINER:
//             newValue = {0, 1}       referenceValue = {0, 1}
//                        /    \                         /   \
//                     += 1 ; += -1                   *= -1  *= 1
        numberOfCellsToFill += (1 - newValue*2) * (-1 + referenceValue*2);

//      BORING SOLUTION :)
//        if (referenceValue == 0) {
//            if (newValue == 0) {
//                numberOfCellsToFill -= 1;
//            } else {
//                numberOfCellsToFill += 1;
//            }
//        } else if (referenceValue == 1) {
//            if (newValue == 1) {
//                numberOfCellsToFill -= 1;
//            } else {
//                numberOfCellsToFill += 1;
//            }
//        }

        if (numberOfCellsToFill == 0 && isLevelFilledCorrectly(level.getLevelData(), currentLevelView)) {
            isGameOver = true;
            actionListener.onPlayerWon(timeSupplier.get() - startTime);
        }
    }

    public void onCellHighlighted(int rowIdx, int colIdx) {
        if (isGameOver) {
            return;
        }

        actionListener.onCellHighlighted(rowIdx, colIdx);
    }

    boolean isLevelFilledCorrectly(final int[][] referenceLevel, final int[][] compareLevel) {
        return Arrays.deepEquals(referenceLevel, compareLevel);
    }

    int countNumberOfFilledCells(final int[][] levelData) {
        return Stream.of(levelData).flatMapToInt(IntStream::of).sum();
    }

}
