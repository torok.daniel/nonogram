package nonogram.model;

public interface GameModelActionListener {

    void onCellSelected(final int rowIdx, final int colIdx, final int newValue);
    void onCellHighlighted(final int rowIdx, final int colIdx);
    void onPlayerWon(final long elapsedTime);
    void onDrawInitialLevel(final int[][] rowInformation, final int[][] colInformation);

}
