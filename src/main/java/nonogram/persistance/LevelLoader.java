package nonogram.persistance;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LevelLoader {

    public LevelLoader() {

    }

    /**
     * Loads the Level from the provided InputStream. The Level must contain
     * Integer numbers without spaces in a NxN matrix form.
     *
     * For example:
     * 0100
     * 0010
     *
     * @param inputStream the source
     * @return Level
     */
    public Level loadLevel(final InputStream inputStream) {
        final List<List<Integer>> dynamicLevelData = readLevelDataDynamically(inputStream);

        final int[][] levelData = convertDynamicLevelData(dynamicLevelData);
        final int[][] rowInformation = LevelUtilities.getRowInformation(levelData);
        final int[][] colInformation = LevelUtilities.getColInformation(levelData);

        return new Level(levelData, rowInformation, colInformation);
    }

    List<List<Integer>> readLevelDataDynamically(final InputStream inputStream) {
        final List<List<Integer>> levelData = new ArrayList<>();
        final Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNextLine()) {
            final String line = scanner.nextLine();
            final List<Integer> row = Stream
                    .of(line.split(""))
                    .map(Integer::valueOf)
                    .collect(Collectors.toList());
            levelData.add(row);
        }
        return levelData;
    }

    int[][] convertDynamicLevelData(final List<List<Integer>> dynamicLevelData) {
        final int[][] levelData = new int[dynamicLevelData.size()][dynamicLevelData.get(0).size()];
        for (int rowIdx = 0; rowIdx < dynamicLevelData.size(); rowIdx++) {
            final List<Integer> row = dynamicLevelData.get(rowIdx);
            for (int colIdx = 0; colIdx < row.size(); colIdx++) {
                levelData[rowIdx][colIdx] = row.get(colIdx);
            }
        }
        return levelData;
    }

}
