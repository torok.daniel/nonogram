package nonogram.persistance;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LevelUtilities {

    /**
     * Counts the number of continuous '1's in the provided list, separated by '0's.
     * [0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1] -> [2, 3, 1]
     * @param list
     * @return
     */
    public static int[] getNumberOfContinuousCells(final int[] list) {
        final String textForm = IntStream.of(list).mapToObj(String::valueOf).collect(Collectors.joining(""));
        return Stream
                .of(textForm.split("0")) // 0011011101                     -> ["", "11", "", "111", "", "1"]
                .filter(s -> !s.isEmpty())     // ["", "11", "", "111", "", "1"] -> ["11", "111", "1"]
                .mapToInt(String::length)      // ["11", "111", "1"]             -> [2, 3, 1]
                .toArray();
    }

    public static int[][] getRowInformation(final int[][] levelData) {
        return Stream.of(levelData)
                .map(LevelUtilities::getNumberOfContinuousCells)
                .toArray(int[][]::new);
    }

    public static int[][] getColInformation(final int[][] levelData) {
        final int columnCount = levelData[0].length;
        return IntStream.range(0, columnCount)
                .mapToObj(colIdx -> getColumnFromMatrix(levelData, colIdx))
                .map(LevelUtilities::getNumberOfContinuousCells)
                .toArray(int[][]::new);
    }


    /**
     * Retrieves a full column from a matrix.
     * 0 1 01
     * 0 1 11
     * 0 0 00  ->  [1, 1, 0] if colIdx was 1
     * @param matrix
     * @param colIdx
     * @return
     */
    static int[] getColumnFromMatrix(final int[][] matrix, int colIdx) {
        final int[] column = new int[matrix.length];
        for (int rowIdx = 0; rowIdx < matrix.length; rowIdx++) {
            column[rowIdx] = matrix[rowIdx][colIdx];
        }
        return column;
    }


}
