package nonogram.persistance;

public class Level {

    private final int[][] levelData;
    private final int[][] rowInformation;
    private final int[][] colInformation;

    public Level(final int[][] levelData, final int[][] rowInformation, final int[][] colInformation) {
        this.levelData = levelData;
        this.rowInformation = rowInformation;
        this.colInformation = colInformation;
    }

    public int[][] getLevelData() {
        // TODO: should copy the List so we don't have "this" escape
        return levelData;
    }

    public int[][] getRowInformation() {
        return rowInformation;
    }

    public int[][] getColInformation() {
        return colInformation;
    }

    public int getWidth() {
        return colInformation.length;
    }

    public int getHeight() {
        return rowInformation.length;
    }

}
