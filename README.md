# Nonogram

From [Wikipedia](https://en.wikipedia.org/wiki/Nonogram):

*"Nonograms, also known as Picross, Griddlers, Pic-a-Pix, and various other names,
are picture logic puzzles in which cells in a grid must be colored or left blank
according to numbers at the side of the grid to reveal a hidden picture. In this
puzzle type, the numbers are a form of discrete tomography that measures how many
unbroken lines of filled-in squares there are in any given row or column. For example,
a clue of "4 8 3" would mean there are sets of four, eight, and three filled squares,
in that order, with at least one blank square between successive sets."*